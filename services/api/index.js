// импортируем библиотеки и утилиты
import express from 'express'
import cors from 'cors'
import Prisma from '@prisma/client'
import apiRoutes from './routes/index.js'
import { join, dirname } from 'path'
import { fileURLToPath } from 'url'
const { PrismaClient } = Prisma

// создаем и экспортируем экземпляр `prisma`
export const prisma = new PrismaClient()

// путь к текущей директории
const __dirname = dirname(fileURLToPath(import.meta.url))

// создаем экземпляр приложения `express`
const app = express()

// отключаем `cors`
app.use(cors())
// включаем парсинг `json` в объекты
app.use(express.json())

// это пригодится нам при запуске приложения в производственном режиме
if (process.env.ENV === 'prod') {
  // обратите внимание на пути
  // путь к текущей директории + `client/build`
  const clientBuildPath = join(__dirname, 'client', 'build')
  // путь к текущей директории + `admin/dist`
  const adminDistPath = join(__dirname, 'admin', 'dist')

  // обслуживание статических файлов
  // клиент будет доступен по пути сервера
  app.use(express.static(clientBuildPath))
  app.use(express.static(adminDistPath))
  // админка будет доступна по пути сервера + `/admin`
  app.use('/admin', (req, res) => {
    res.sendFile(join(adminDistPath, decodeURIComponent(req.url)))
  })
}
// роутинг
app.use('/api', apiRoutes)

// обработчик ошибок
app.use((err, req, res, next) => {
  console.log(err)
  const status = err.status || 500
  const message = err.message || 'Something went wrong. Try again later'
  res.status(status).json({ message })
})

// запускаем сервер на порту 5000
app.listen(5000, () => {
  console.log(`Server ready 🚀 `)
})